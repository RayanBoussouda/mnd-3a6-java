/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

/**
 *
 * @author kaisf
 */
public class Vote_annonce {

    private int id;
    private int id_user;
    private int id_voteur;
    private int vote;

    public Vote_annonce(int id, int id_user, int id_voteur, int vote) {
        this.id = id;
        this.id_user = id_user;
        this.id_voteur = id_voteur;
        this.vote = vote;
    }

    @Override
    public String toString() {
        return "id: "+id+"id_user: "+id_user+"id_voteur: "+id_voteur+"vote: "+vote;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_user
     */
    public int getId_user() {
        return id_user;
    }

    /**
     * @param id_user the id_user to set
     */
    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    /**
     * @return the id_voteur
     */
    public int getId_voteur() {
        return id_voteur;
    }

    /**
     * @param id_voteur the id_voteur to set
     */
    public void setId_voteur(int id_voteur) {
        this.id_voteur = id_voteur;
    }

    /**
     * @return the vote
     */
    public int getVote() {
        return vote;
    }

    /**
     * @param vote the vote to set
     */
    public void setVote(int vote) {
        this.vote = vote;
    }

}
