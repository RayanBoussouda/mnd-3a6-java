/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mnd.entities;

import java.util.List;

/**
 *
 * @author Ahmed
 */
public class Endroit {
    private int id_endroit; 
    private int id_user ;
    private int id_commentaire; 
    private String titre ;
    private String image_url ; 
    private double note ; 
    private List<Commentaire> commentaires ; 
    private String description ; 
    private String pays ;
    private String address ; 
    private String type ;



    public Endroit(int id_user, String titre, String image_url, String description, String pays, String address, String type) {
        this.id_user= id_user; 
        this.titre = titre;
        this.image_url = image_url;
        this.description = description;
        this.pays = pays ; 
        this.address = address;
        this.type = type; 
    }
    
    public Endroit( int id_endroit, int id_user, String titre, String image_url, String description, String pays, String address, String type) {
        this.id_endroit = id_endroit;
        this.id_user= id_user; 
        this.titre = titre;
        this.image_url = image_url;
        this.description = description;
        this.pays = pays ; 
        this.address = address;
        this.type = type; 
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public List<Commentaire> getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(List<Commentaire> commentaires) {
        this.commentaires = commentaires;
    }

    
    public int getId_endroit() {
        return id_endroit;
    }

    public void setId_endroit(int id_endroit) {
        this.id_endroit = id_endroit;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_commentaire() {
        return id_commentaire;
    }

    public void setId_commentaire(int id_commentaire) {
        this.id_commentaire = id_commentaire;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

 

    

    @Override
    public String toString() {
        return "Endroit{" + "id_endroit=" + id_endroit + ", id_user=" + id_user + ", id_commentaire=" + id_commentaire + ", titre=" + titre + ", image_url=" + image_url + ", note=" + note + ", commentaires=" + commentaires + ", description=" + description + ", pays=" + pays + ",address"+address+ ",type"+ type+'}';
    }

   

    
    
    
    
}
