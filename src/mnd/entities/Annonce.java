/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;


/**
 *
 * @author kaisf
 */
public class Annonce {
    private int id;
    private int id_user;
    private String date_annonce;
    private String description;
    private int nb_personne;
    private String sexe;
    private int age;
    private String date_expiration;
    private String image_url;
    private String status;
    private String pays;


    public Annonce(){
        
    }
    public Annonce(int id,int id_user,String date_annonce,String description,int nb_personne,String sexe,int age,String date_expiration,String image_url,String status,String pays){
    this.id=id;
    this.id_user=id_user;
    this.date_annonce=date_annonce;
    this.description=description;
    this.nb_personne=nb_personne;
    this.sexe=sexe;
    this.age=age;
    this.date_expiration=date_expiration;
    this.image_url=image_url;
    this.status=status;
    this.pays=pays;

    }
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_user
     */
    public int getId_user() {
        return id_user;
    }

    /**
     * @param id_user the id_user to set
     */
    public void setId_user(int id_user) {
        this.id_user = id_user;
    }


    /**
     * @return the date_annonce
     */
    public String getDate_annonce() {
        return date_annonce;
    }

    /**
     * @param date_annonce the date_annonce to set
     */
    public void setDate_annonce(String date_annonce) {
        this.date_annonce = date_annonce;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the nb_personne
     */
    public int getNb_personne() {
        return nb_personne;
    }

    /**
     * @param nb_personne the nb_personne to set
     */
    public void setNb_personne(int nb_personne) {
        this.nb_personne = nb_personne;
    }

    /**
     * @return the sexe
     */
    public String getSexe() {
        return sexe;
    }

    /**
     * @param sexe the sexe to set
     */
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the date_expiration
     */
    public String getDate_expiration() {
        return date_expiration;
    }

    /**
     * @param date_expiration the date_expiration to set
     */
    public void setDate_expiration(String date_expiration) {
        this.date_expiration = date_expiration;
    }

    /**
     * @return the image_url
     */
    public String getImage_url() {
        return image_url;
    }

    /**
     * @param image_url the image_url to set
     */
    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }
     @Override
    public String toString() {
        return "Annonce{" + "id=" + getId() + ", id_user=" + getId_user() + ",date_annonce="+getDate_annonce()+",description="+getDescription()+",nb_personne="+getNb_personne()+",sexe="+getSexe()+",age="+getAge()+",date_expiration="+getDate_expiration()+",image_url="+getImage_url()+",status="+getStatus()+",pays="+pays+'}';
    }

    /**
     * @return the pays
     */
    public String getPays() {
        return pays;
    }

    /**
     * @param pays the pays to set
     */
    public void setPays(String pays) {
        this.pays = pays;
    }

}
