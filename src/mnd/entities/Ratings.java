/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.entities;

/**
 *
 * @author Steve
 */
public class Ratings {
    private int id_user;
    private int id_evenement;
    private double valeur;

    public Ratings(int id_user, int id_evenement, double valeur) {
        this.id_user = id_user;
        this.id_evenement = id_evenement;
        this.valeur = valeur;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_evenement() {
        return id_evenement;
    }

    public void setId_evenement(int id_evenement) {
        this.id_evenement = id_evenement;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    @Override
    public String toString() {
        return "Ratings{" + "id_user=" + id_user + ", id_evenement=" + id_evenement + ", valeur=" + valeur + '}';
    }
    
    
    
}
