/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jnlp.PrintService;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.print.PrintServiceLookup;
import mnd.entities.Evenement;
import mnd.services.EvenementServices;
import org.controlsfx.control.Rating;
import mnd.entities.Ratings;
import mnd.entities.User;


/**
 * FXML Controller class
 *
 * @author Steve
 */
public class DetailsEvenementController implements Initializable {

    public static int id;
    @FXML
    private Label titre;
    @FXML
    private Label lieux;
    @FXML
    private Label nbr;
    @FXML
    private Label date;
    @FXML
    private JFXTextArea descrip;
    @FXML
    private Button bt1;
    @FXML
    private ImageView imageevent1;
    @FXML
    private Button supprimerev;
    @FXML
    private JFXButton participer;
    @FXML
    private Rating rating;
    @FXML
    private TableView<User> table;
    EvenementServices es = new EvenementServices();
    private final ObservableList<User> data = FXCollections.observableArrayList(es.listRes(id));
    @FXML
    private ImageView joinus;
   

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.setEditable(true);

        TableColumn nomUser = new TableColumn("Nom Participant");
        TableColumn emailUser = new TableColumn("Email");
      
        nomUser.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
        emailUser.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
        table.setItems(data);
	emailUser.setPrefWidth(200);

        table.getColumns().addAll(nomUser, emailUser);
        rating.ratingProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                Ratings r = new Ratings(AuthentificationController.getInstance().getUser().getId(), id, (double) newValue);
                EvenementServices es = new EvenementServices();
                es.rating(r);
            }

        });
//        profileLabel.setText(mnd.main.Mnd.getInstance().getLoggedUser().getUsername());

        EvenementServices es = new EvenementServices();
        Evenement ev = es.findById(id);

        String url1 = ev.getImage_url();
        File file = new File("C:\\wamp64\\www\\image\\" + url1);
        Image imageEvent = new Image(file.toURI().toString());
        // ImageView imgview = new ImageView(imageEvent);
        imageevent1.setFitHeight(500);
        imageevent1.setFitWidth(400);
        Rectangle clip = new Rectangle(
                imageevent1.getFitWidth(), imageevent1.getFitHeight()
        );

        clip.setArcWidth(150);
        clip.setArcHeight(80);
        imageevent1.setClip(clip);

        // snapshot the rounded image.
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        WritableImage image = imageevent1.snapshot(parameters, null);

        // remove the rounding clip so that our effect can show through.
        imageevent1.setClip(null);

        // apply a shadow effect.
        imageevent1.setEffect(new DropShadow(20, Color.BLACK));

        imageevent1.setImage(imageEvent);

        System.out.println(ev);
        titre.setText(ev.getTitre());
        lieux.setText(ev.getPays());
        nbr.setText(ev.getNbr_participant() + "");
        descrip.setText(ev.getDescription());

        date.setText(ev.getDate() + "");

        if (ev.getId_user() == AuthentificationController.getInstance().getUser().getId()) {
//        if(ev.getId_user()==mnd.main.Mnd.getInstance().getLoggedUser().getId()){
            // bt1.setText("Modifier");
            participer.setVisible(false);
            rating.setVisible(false);
            joinus.setVisible(false);
           
        } else {
            boolean testR=es.testrating(id,AuthentificationController.getInstance().getUser().getId());
            if(testR){
                participer.setText("Deja Participant");
                participer.setDisable(true);
                
               
            }
            
            //bt1.setText("S'inscrire");
            supprimerev.setVisible(false);
            bt1.setVisible(false);
            table.setVisible(false);
           
            
            
           

        }

    }

    public void redirect(String id) {
        System.out.println("Interface Affiche Details");
        System.out.println(id);
        this.id = Integer.parseInt(id);

    }

    @FXML
    private void modifier(ActionEvent event) {
        ModifierEvenementController l = new ModifierEvenementController();
        l.redirect(String.valueOf(id));
        try {
            //  Stage stage = (Stage) bt1.getScene().getWindow();
            // do what you have to do
            //stage.close();

            Parent root = null;
            root = FXMLLoader.load(getClass().getResource("/mnd/gui/ModifierEvenement.fxml"));
            Stage stage1 = new Stage();
            Scene scene = new Scene(root);
            stage1.setScene(scene);
            stage1.show();

            // mnd.main.Mnd.getInstance().getStage().hide();
            //mnd.main.Mnd.getInstance().ChangeScene(new Scene(FXMLLoader.load(getClass().getResource("/mnd/gui/ModifierEvenement.fxml"))));
            //mnd.main.Mnd.getInstance().getStage().show();
        } catch (IOException ex) {
            Logger.getLogger(DetailsEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void supprimerevenement(ActionEvent event) throws IOException {
        EvenementServices es = new EvenementServices();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Annulation de l'évènement");
        alert.setHeaderText(null);
        alert.setContentText("Voulez vous annuler l'évènement !");
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            es.remove(es.findById(id));
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setTitle("Annulation de l'évènement");
            alert2.setHeaderText(null);
            alert2.setContentText("Votre évènement a étè annulé !");
            alert2.showAndWait();
            //Stage stage = (Stage) bt1.getScene().getWindow();
            // do what you have to do
            //   stage.close();
            Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/AfficherEvenement.fxml"));
            UiController.borderPaneS.setCenter(root);

        }
    }

    @FXML
    private void participerevenement(ActionEvent event) throws IOException, PrinterException, DocumentException, MessagingException {
        EvenementServices es = new EvenementServices();
        Evenement e = es.findById(id);
        es.addRes(id, AuthentificationController.getInstance().getUser().getId());
        try {

            //construct the text body part
            Document d = new Document();

            String shemain = "C:/Users/Steve/Documents/NetBeansProjects/" + e.getTitre() + AuthentificationController.getInstance().getUser().getUsername() + ".pdf";
            String p = "********************************************************\n "
                    + " Vous etes inscrit à  \n  "
                    + "                                  " + e.getTitre() + " \n "+"\n"+"\n"+"\n"
                    + "Description de l'évènement  \n:"+"                            " + e.getDescription() + " \n "+"\n"
                    + "Date   :   " + e.getDate() + " \n "+"\n"
                    + "Pays   :   " + e.getPays() + " \n "
                    +" \n"+" \n"+" \n"+" \n"
                    +" \n"+" \n"+" \n"
                    +"Merci pour votre confiance.\n"
                    +"Consulter toujours notre plateforme M&D pour plus d'évènement"
                    + "********************************************************\n ";

            PdfWriter.getInstance(d, new FileOutputStream(shemain));
            d.open();
            d.add(new Paragraph(p));
            d.close();

            String to = AuthentificationController.getInstance().getUser().getEmail();

            // Sender's email ID needs to be mentioned
            final String username = "mnd.saif.123456@gmail.com";
            final String password = "saif123456";
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("mnd.saif.123456@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            message.setSubject("Participation à un évènement");

            //3) create MimeBodyPart object and set your message text        
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText("This is message body");

            //4) create new MimeBodyPart object and set DataHandler object to this object        
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
            String filename = "C:/Users/Steve/Documents/NetBeansProjects/" + e.getTitre() + AuthentificationController.getInstance().getUser().getUsername() + ".pdf";//change accordingly     
            DataSource source = new FileDataSource(filename);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(filename);

            //5) create Multipart object and add MimeBodyPart objects to this object        
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart1);
            multipart.addBodyPart(messageBodyPart2);

            //6) set the multiplart object to the message object    
            message.setContent(multipart);

            //7) send message    
            Transport.send(message);

            System.out.println("Done");
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Participation reussite");
                alert.setHeaderText(null);
                alert.setContentText("Vous avez recu un mail de participation à "+e.getTitre());

                alert.showAndWait();
                Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/DetailsEvenement.fxml"));
                                        UiController.borderPaneS.setCenter(root);
                                     

        } catch (DocumentException | FileNotFoundException ex) {
            System.out.println(ex.getMessage());

        }
    }

     
    


}
