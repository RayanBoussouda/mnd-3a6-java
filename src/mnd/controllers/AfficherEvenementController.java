/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import mnd.entities.Evenement;
import mnd.services.EvenementServices;
import javafx.scene.SnapshotParameters;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import mnd.controllers.DetailsEvenementController;

/**
 * FXML Controller class
 *
 * @author Steve
 */
public class AfficherEvenementController implements Initializable {

    @FXML
    private ListView<Evenement> list;

    private ObservableList<Evenement> data;
    @FXML
    private AnchorPane rootpane;
    @FXML
    private Button intrfaceajout;
    private Label profileLabel;
    @FXML
    private JFXButton filtrer;
    @FXML
    private JFXTextField searchField;
    @FXML
    private JFXComboBox<String> pays;
    ObservableList<String> Pays
            = FXCollections.observableArrayList("Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre", "Tous les pays");
    List<Evenement> evenements = new ArrayList<Evenement>();
    ObservableList<Evenement> items = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pays.setItems(Pays);
        // TODO
//            profileLabel.setText(mnd.main.Mnd.getInstance().getLoggedUser().getUsername());

        EvenementServices es = new EvenementServices();

        evenements = es.findAll();
        refreshList(evenements);
        
       

    }

    public void refreshList(List<Evenement> evenements) {

        items.setAll(evenements);
        list.setCellFactory((ListView<Evenement> arg0) -> {
            ListCell<Evenement> cell;
            cell = new ListCell<Evenement>() {
                @Override
                protected void updateItem(Evenement e, boolean btl) {
                    super.updateItem(e, btl);

                    if (e != null) {
                        //    File file = new File(ps.findById(e.getPassager().getId()).getAvatar());
                        String url = e.getImage_url();
                        File file = new File("C:\\wamp64\\www\\image\\" + url);
                        Image imageEvent = new Image(file.toURI().toString());
                        // File file = new File("src\\ayy.JPG");
                        // file.getParentFile().mkdirs();
                        // Image IMAGE_RUBY = new Image(file.toURI().toString());
                        //   Image IMAGE_RUBY = new Image(ps.findById(e.getPassager().getId()).getAvatar());

                        ImageView imgview = new ImageView(imageEvent);

                        setGraphic(imgview);

                        imgview.setFitHeight(200);
                        imgview.setFitWidth(200);
                        Rectangle clip = new Rectangle(
                                imgview.getFitWidth(), imgview.getFitHeight()
                        );

                        clip.setArcWidth(90);
                        clip.setArcHeight(90);
                        imgview.setClip(clip);

                        // snapshot the rounded image.
                        SnapshotParameters parameters = new SnapshotParameters();
                        parameters.setFill(Color.TRANSPARENT);
                        WritableImage image = imgview.snapshot(parameters, null);

                        // remove the rounding clip so that our effect can show through.
                        imgview.setClip(null);

                        // apply a shadow effect.
                        imgview.setEffect(new DropShadow(20, Color.BLACK));

                        // store the rounded image in the imageView.
                        imgview.setImage(image);

                        //   TextField id = new TextField(String.valueOf(e.getId()));
                        // System.out.println(id);
                        //id.setVisible(true);
                        setId(String.valueOf(e.getId()));
                        list.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {
                                if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 1) {
                                    Evenement ev = list.getItems().get(list.getSelectionModel().getSelectedIndex());
                                    System.out.println(ev);
                                    DetailsEvenementController l = new DetailsEvenementController();
                                    l.redirect(String.valueOf(ev.getId()));
                                    System.out.println(ev.getId());
                                    AnchorPane pane = new AnchorPane();
                                    try {
                                        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/DetailsEvenement.fxml"));
                                        UiController.borderPaneS.setCenter(root);
                                    } catch (IOException ex) {
                                        Logger.getLogger(ModifierEvenementController.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    rootpane.getChildren().setAll(pane);
                                }
                            }

                        });

                        // if(re.getType())
                        setText("     Titre     : " + e.getTitre() + "\n" + "\n      Localisation   : " + e.getPays() + "            " + "      " + "  Nombre de participants   : " + e.getNbr_participant() + "\n" + " \n      Date   : " + e.getDate() + "                        " + " Description   : " + e.getDescription());

                        setFont(Font.font("Berlin Sans FB Demi Bold", 20));

                        // setAlignment(Pos.CENTER);
                    }

                }

            };
            return cell;
        });
        list.setItems(items);
    }

    @FXML
    private void ajoutev(ActionEvent event) {
        AjouterEvenementController l = new AjouterEvenementController();

        try {

            Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/AjouterEvenement.fxml"));
            Stage stage = new Stage();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(DetailsEvenementController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void filtrer(ActionEvent event) {
        List<Evenement> list = new ArrayList<>();

        if (!searchField.getText().equals("") || !pays.getValue().equals("") || !pays.getValue().equals("Tous les pays")) {
            if (searchField.getText().equals("") && pays.getValue().equals("")) {
                // list = es.filterByTitle(searchField.getText());
                list = evenements.stream().filter(x -> x.getTitre().equals(searchField.getText())).collect(Collectors.toList());
            }
            if (searchField.getText().equals("") && (!pays.getValue().equals("") || !pays.getValue().equals("Tous les pays"))) {
                list = evenements.stream().filter(x -> x.getPays().equals(pays.getValue())).collect(Collectors.toList());
                // list = es.getByCountry(pays.getValue());
            }
            if (!searchField.getText().equals("") && (!pays.getValue().equals("") || !pays.getValue().equals("Tous les pays"))) {
                list = evenements.stream().filter(x -> x.getPays().equals(pays.getValue())).filter(y -> y.getTitre().equals(searchField.getText())).collect(Collectors.toList());

            }
            if (searchField.getText().equals("") && pays.getValue().equals("Tous les pays")) {
                list = evenements.stream().collect(Collectors.toList());
            }
            if (!searchField.getText().equals("") && pays.getValue().equals("Tous les pays")) {
                list = evenements.stream().filter(x -> x.getTitre().equals(searchField.getText())).collect(Collectors.toList());
            }
        }

        refreshList(list);

    }
}
