/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import mnd.entities.Status;
import mnd.entities.User;
import mnd.services.StatusService;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author Camilia
 */
public class AdminHomeController implements Initializable {

    @FXML
    private BarChart<?, ?> userAge;
    @FXML
    private NumberAxis y;
    @FXML
    private CategoryAxis x;
    User u;
    UserService us;
    @FXML
    private BarChart<?, ?> userCountry;
    @FXML
    private NumberAxis y2;
    @FXML
    private CategoryAxis x2;
    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> NomC;
    @FXML
    private TableColumn<User, String> EmailC;
    @FXML
    private TableColumn<User, Boolean> BanC;
    @FXML
    private TableColumn<User, String> RoleC;
    @FXML
    private TableColumn<User, String> GenreC;
    @FXML
    private TableColumn<User, Integer> Numero_MobileC;
    @FXML
    private TableColumn<User, String> PaysC;
    @FXML
    private TableColumn<User, Integer> AgeC;
    private ObservableList<User> ul;
    @FXML
    private Button banB;
     int UserSelected;


 

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            refresh();
        } catch (SQLException ex) {
            Logger.getLogger(AdminHomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<User>     lu = new ArrayList<>();
        List<Integer>  la = new ArrayList<>(); 
        List<String>  lc = new ArrayList<>(); 
        try {
            us = new UserService();
        } catch (SQLException ex) {
            Logger.getLogger(AdminHomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        lu = us.getAllUsers();         
    //............BAR CHART FOR USER AGE............//    
        XYChart.Series set1 = new XYChart.Series<>();
        for (int i=0;i<lu.size();i++){
            la.add(lu.get(i).getAge());
        }
        Map<Integer, Long> counts = la.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        counts.forEach((k,v)->set1.getData().add(new XYChart.Data(Integer.toString(k),v)));
        userAge.getData().addAll(set1);
        
    //............BAR CHART FOR USER COUNTRY............//
       
        XYChart.Series set2 = new XYChart.Series<>();
        for (int i=0;i<lu.size();i++){
            lc.add(lu.get(i).getCountry());
        }
        Map<String, Long> counts_country = lc.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        counts_country.forEach((k,v)->set2.getData().add(new XYChart.Data(k,v)));
        userCountry.getData().addAll(set2);
  }
    public void refresh() throws SQLException{
        us = new UserService();
        ul= FXCollections.observableArrayList(us.getAllUsers());
        NomC.setCellValueFactory(new PropertyValueFactory<>("username"));
        EmailC.setCellValueFactory(new PropertyValueFactory<>("email"));
        BanC.setCellValueFactory(new PropertyValueFactory<>("enabled"));
        RoleC.setCellValueFactory(new PropertyValueFactory<>("roles"));
        GenreC.setCellValueFactory(new PropertyValueFactory<>("genre"));
        Numero_MobileC.setCellValueFactory(new PropertyValueFactory<>("phone_number"));
        PaysC.setCellValueFactory(new PropertyValueFactory<>("country"));
        AgeC.setCellValueFactory(new PropertyValueFactory<>("age"));
        userTable.setItems(ul);
    }

    @FXML
    private void getUser(MouseEvent event) throws SQLException {
         us = new UserService();      
         UserSelected=userTable.getSelectionModel().getSelectedItem().getId();
        System.out.println(UserSelected);
    }

    @FXML
    private void banA(ActionEvent event) throws SQLException {
       us = new UserService();  
       u = us.findUserById(UserSelected);
       if(u.getEnabled()==true){
           User u = new User(false);
           u.setId(UserSelected);
           
           us.updateUserEnabled(u);
       }else{
           User u = new User(true);
           
            u.setId(UserSelected);
            us.updateUserEnabled(u);
       }System.out.println(u.getEnabled());
       refresh();
    }
       
   

    }



