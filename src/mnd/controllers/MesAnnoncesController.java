/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import mnd.entities.Annonce;
import mnd.entities.Reservation_annonce;
import mnd.entities.User;
import mnd.services.AnnonceService;
import mnd.services.Reservation_annonceService;
import mnd.services.UserService;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class MesAnnoncesController implements Initializable {

    private final ObservableList<String> listePays = FXCollections.observableArrayList("","Afghanistan", " Albanie", " Antarctique", " Algérie", " Samoa Américaines", " Andorre", " Angola", " Antigua-et-Barbuda", " Azerbaïdjan", " Argentine", " Australie", " Autriche", " Bahamas", " Bahreïn", " Bangladesh", " Arménie", " Barbade", " Belgique", " Bermudes", " Bhoutan", " Bolivie", " Bosnie-Herzégovine", " Botswana", " Île Bouvet", " Brésil", " Belize", " Territoire Britannique de Océan Indien", " Îles Salomon", " Îles Vierges Britanniques", " Brunéi Darussalam", " Bulgarie", " Myanmar", " Burundi", " Bélarus", " Cambodge", " Cameroun", " Canada", " Cap-vert", " Îles Caïmanes", " République Centrafricaine", " Sri Lanka", " Tchad", " Chili", " Chine", " Taïwan", " Île Christmas", " Îles Cocos (Keeling)", " Colombie", " Comores", " Mayotte", " République du Congo", " République Démocratique du Congo", " Îles Cook", " Costa Rica", " Croatie", " Cuba", " Chypre", " République Tchèque", " Bénin", " Danemark", " Dominique", " République Dominicaine", " Équateur", " El Salvador", " Guinée Équatoriale", " Éthiopie", " Érythrée", " Estonie", " Îles Féroé", " Îles (malvinas) Falkland", " Géorgie du Sud et les Îles Sandwich du Sud", " Fidji", " Finlande", " Îles Åland", " France", " Guyane Française", " Polynésie Française", " Terres Australes Françaises", " Djibouti", " Gabon", " Géorgie", " Gambie", " Territoire Palestinien Occupé", " Allemagne", " Ghana", " Gibraltar", " Kiribati", " Grèce", " Groenland", " Grenade", " Guadeloupe", " Guam", " Guatemala", " Guinée", " Guyana", " Haïti", " Îles Heard et Mcdonald", " Saint-Siège (état de la Cité du Vatican)", " Honduras", " Hong-Kong", " Hongrie", " Islande", " Inde", " Indonésie", " République Islamique Iran", " Iraq", " Irlande", " Israël", " Italie", " Côte Ivoire", " Jamaïque", " Japon", " Kazakhstan", " Jordanie", " Kenya", " République Populaire Démocratique de Corée", " République de Corée", " Koweït", " Kirghizistan", " République Démocratique Populaire Lao", " Liban", " Lesotho", " Lettonie", " Libéria", " Jamahiriya Arabe Libyenne", " Liechtenstein", " Lituanie", " Luxembourg", " Macao", " Madagascar", " Malawi", " Malaisie", " Maldives", " Mali", " Malte", " Martinique", " Mauritanie", " Maurice", " Mexique", " Monaco", " Mongolie", " République de Moldova", " Montserrat", " Maroc", " Mozambique", " Oman", " Namibie", " Nauru", " Népal", " Pays-Bas", " Antilles Néerlandaises", " Aruba", " Nouvelle-Calédonie", " Vanuatu", " Nouvelle-Zélande", " Nicaragua", " Niger", " Nigéria", " Niué", " Île Norfolk", " Norvège", " Îles Mariannes du Nord", " Îles Mineures Éloignées des États-Unis", " États Fédérés de Micronésie", " Îles Marshall", " Palaos", " Pakistan", " Panama", " Papouasie-Nouvelle-Guinée", " Paraguay", " Pérou", " Philippines", " Pitcairn", " Pologne", " Portugal", " Guinée-Bissau", " Timor-Leste", " Porto Rico", " Qatar", " Réunion", " Roumanie", " Fédération de Russie", " Rwanda", " Sainte-Hélène", " Saint-Kitts-et-Nevis", " Anguilla", " Sainte-Lucie", " Saint-Pierre-et-Miquelon", " Saint-Vincent-et-les Grenadines", " Saint-Marin", " Sao Tomé-et-Principe", " Arabie Saoudite", " Sénégal", " Seychelles", " Sierra Leone", " Singapour", " Slovaquie", " Viet Nam", " Slovénie", " Somalie", " Afrique du Sud", " Zimbabwe", " Espagne", " Sahara Occidental", " Soudan", " Suriname", " Svalbard etÎle Jan Mayen", " Swaziland", " Suède", " Suisse", " République Arabe Syrienne", " Tadjikistan", " Thaïlande", " Togo", " Tokelau", " Tonga", " Trinité-et-Tobago", " Émirats Arabes Unis", " Tunisie", " Turquie", " Turkménistan", " Îles Turks et Caïques", " Tuvalu", " Ouganda", " Ukraine", "ex-République Yougoslave de Macédoine", " Égypte", " Royaume-Uni", " Île de Man", " République-Unie de Tanzanie", " États-Unis", " Îles Vierges des États-Unis", " Burkina Faso", " Uruguay", " Ouzbékistan", " Venezuela", " Wallis et Futuna", " Samoa", " Yémen", " Serbie-et-Monténégro", " Zambie");
    int id_annonce;
    String imageUrl = "";
    ArrayList<Reservation_annonce> reservationsAnnonces;
    User u = AuthentificationController.getInstance().getUser();
    int page = 0;
    int max;
    @FXML
    private AnchorPane ap;
    @FXML
    private Button modifier;
    @FXML
    private JFXComboBox<String> pays;
    @FXML
    private RadioButton masculin;
    @FXML
    private RadioButton feminin;
    @FXML
    private JFXDatePicker expiration;
    @FXML
    private JFXTextArea description;
    @FXML
    private Label error;
    @FXML
    private Label notif;
    @FXML
    private JFXTextField age;
    @FXML
    private JFXTextField personne;
    @FXML
    private Button effacer;
    @FXML
    private GridPane grid;
    @FXML
    private Button retour;
    @FXML
    private Button suivant;
    @FXML
    private Button photo;
    @FXML
    private ImageView imageview;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            pays.setItems(listePays);
            Reservation_annonceService reservserv = new Reservation_annonceService();
            AnnonceService serv = new AnnonceService();
            id_annonce = serv.getAnnonceParuserid(u.getId()).get(0).getId();
            Annonce annonce = serv.getAnnonceByid(id_annonce);
            imageUrl = annonce.getImage_url();
            pays.setValue(annonce.getPays());
            expiration.setValue(LocalDate.parse(annonce.getDate_expiration()));
            age.setText(Integer.toString(annonce.getAge()));
            personne.setText(Integer.toString(annonce.getNb_personne()));
            description.setText(annonce.getDescription());
            if (annonce.getSexe().equals("both")) {
                masculin.setSelected(true);
                feminin.setSelected(true);

            } else if (annonce.getSexe().equals("masculin")) {
                masculin.setSelected(true);
            } else {
                feminin.setSelected(true);
            }

            reservationsAnnonces = reservserv.getReservationsByIdAnnonce(id_annonce);
            max = reservationsAnnonces.size();
            updateGrid(reservationsAnnonces);
            Image image = new Image(new URL("http://localhost:/pi/images/" + annonce.getImage_url()).openStream());
            imageview.setImage(image);
        } catch (SQLException ex) {
            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateGrid(ArrayList<Reservation_annonce> reservations) throws FileNotFoundException {
        try {
            UserService userserv = new UserService();
            grid.getChildren().clear();
            for (int i = 0 + 4 * page; i < reservations.size() && i < 4 + 4 * page; i++) {
                final int id = reservations.get(i).getId();
                final int ii = i;
                FontAwesomeIcon check = new FontAwesomeIcon();
                check.setIconName("CHECK");
                check.setSize("35");
                check.setFill(Color.GREEN);
                FontAwesomeIcon times = new FontAwesomeIcon();
                times.setIconName("TIMES");
                times.setSize("35");
                times.setFill(Color.RED);
                check.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        check.setFill(Color.GREENYELLOW);
                    }
                });
                check.setOnMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        check.setFill(Color.GREEN);
                    }
                });
                check.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        try {
                            try {
                                UserService userservice = new UserService();
                                Reservation_annonceService re = new Reservation_annonceService();
                                int id_user = reservationsAnnonces.get(ii).getId_user();
                                final String username = "nodignitypi@gmail.com";
                                final String password = "nodignity123";
                                Properties props = new Properties();
                                props.put("mail.smtp.auth", "true");
                                props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
                                props.put("mail.smtp.starttls.enable", "true");
                                props.put("mail.smtp.host", "smtp.gmail.com");
                                props.put("mail.smtp.port", "587");
                                re.deleteByIdAnnonce(id_annonce);
                                AnnonceService serv = new AnnonceService();
                                serv.supprimerAnnonceParid(id_annonce);
                                Session session = Session.getInstance(props,
                                        new javax.mail.Authenticator() {
                                    protected PasswordAuthentication getPasswordAuthentication() {
                                        return new PasswordAuthentication(username, password);
                                    }

                                });

                                try {

                                    Message message = new MimeMessage(session);
                                    message.setFrom(new InternetAddress("nodignitypi@gmail.com"));
                                    message.setRecipients(Message.RecipientType.TO,
                                            InternetAddress.parse(userservice.findUserById(id_user).getEmail()));
                                    message.setSubject("demande accepté");
                                    message.setText("L'utilisateur " + u.getUsername() + " a accepté votre demande\nVous pouvez le contacter maintenand directement avec son email " + u.getEmail());
                                    Transport.send(message);
                                    System.out.println("Done");

                                } catch (MessagingException e) {
                                    throw new RuntimeException(e);
                                }
                            } catch (SQLException ex) {
                                Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            Parent root2 = FXMLLoader.load(getClass().getResource("/mnd/gui/annonces.fxml"));
                            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mnd/gui/Ui.fxml"));
                            Parent root = (Parent) fxmlLoader.load();
                            UiController controller = fxmlLoader.<UiController>getController();
                            controller.setParent(root2);
                            Scene scene = new Scene(root);
                            Stage stage = (Stage) ap.getScene().getWindow();
                            stage.setScene(scene);
                            stage.show();
                        } catch (IOException ex) {
                            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });
                times.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        times.setFill(Color.ORANGE);
                    }
                });
                times.setOnMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        times.setFill(Color.RED);
                    }
                });
                times.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        try {
                            Reservation_annonceService re = new Reservation_annonceService();
                            re.deleteById(id);
                            reservationsAnnonces.remove(ii);
                            updateGrid(reservationsAnnonces);
                        } catch (SQLException ex) {
                            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(MesAnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }
                });
                final Label f = new Label(userserv.findUserById(reservations.get(i).getId_user()).getUsername());
                System.out.print(reservations.get(i));
                final Label f2=new Label(reservations.get(i).getDate_reservation()+"->"+reservations.get(i).getDate_expiration());
                f2.setStyle("-fx-font-size: 10pt;");
                f.setStyle("-fx-text-fill:#100BA4;-fx-font-size: 13pt;");
            f.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    f.setStyle("-fx-text-fill:#5381A4;-fx-underline: true;-fx-font-size: 13pt;");
                }
            });
            f.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    f.setStyle("-fx-text-fill:#100BA4;-fx-underline: false;-fx-font-size: 13pt;");
                }
            });
            f.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    try {
                        UserService userserv = new UserService();
                        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("/mnd/gui/Acueil.fxml"));
                        fxmlLoader2.load();
                        AcueilController controller2 = fxmlLoader2.<AcueilController>getController();
                        controller2.setUser(userserv.findUserByUsername(f.getText()));
                        Parent root = FXMLLoader.load(getClass().getResource("/mnd/gui/Profile.fxml"));
                        Stage stage = (Stage) ap.getScene().getWindow();
                        stage.close();
                        stage = new Stage();
                        Scene scene = new Scene(root);
                        scene.setFill(Color.TRANSPARENT);
                        stage.setScene(scene);
                        stage.initStyle(StageStyle.TRANSPARENT);
                        stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            });                String photourl = userserv.findUserById(reservations.get(i).getId_user()).getPhoto_membre();
                Image image;
                if (photourl == null || photourl == "") {
                    image = new Image(new URL("http://localhost:/pi/images/default.jpg").openStream());
                } else {
                    image = new Image(new URL("http://localhost:/pi/images/" + photourl).openStream());
                }
                ImageView imageView = new ImageView(image);
                imageView.setFitHeight(80);
                imageView.setFitWidth(100);
                GridPane.setConstraints(imageView, 0, (i - 4 * page));
                grid.getChildren().add(imageView);
                GridPane.setConstraints(f, 1, (i - 4 * page));
                grid.getChildren().add(f);
                GridPane.setConstraints(f2, 2, (i - 4 * page));
                grid.getChildren().add(f2);
                GridPane.setConstraints(times, 3, (i - 4 * page));
                grid.getChildren().add(times);
                 GridPane.setConstraints(check, 4, (i - 4 * page));
                grid.getChildren().add(check);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RechercherAnnonceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void next(ActionEvent event) throws FileNotFoundException {
        if ((page + 1) * 4 < max) {
            page += 1;
            updateGrid(reservationsAnnonces);
        }
    }

    @FXML
    private void previous(ActionEvent event) throws FileNotFoundException {
        if (page > 0) {
            page -= 1;
            updateGrid(reservationsAnnonces);
        }
    }

    @FXML
    private void upload(ActionEvent event) throws SQLException, IOException {
        ImageView imgView = new ImageView();//setting imageview where we will set our uploaded picture before taking it to the database
        FileChooser fc = new FileChooser();
        FileChooser.ExtensionFilter ext1 = new FileChooser.ExtensionFilter("JPG files(*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter ext2 = new FileChooser.ExtensionFilter("PNG files(*.png)", "*.PNG");
        fc.getExtensionFilters().addAll(ext1, ext2);
        Stage primarystage = (Stage) ap.getScene().getWindow();
        File source = fc.showOpenDialog(primarystage);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httppost = new HttpPost("http://localhost/pi/upload.php");
            File newFile = new File(source.getParent(), Integer.toString(u.getId()) + source.getName());
            Files.copy(source.toPath(), newFile.toPath());
            FileBody bin = new FileBody(newFile);
            HttpEntity reqEntity = MultipartEntityBuilder.create()
                    .addPart("fileToUpload", bin)
                    .build();

            httppost.setEntity(reqEntity);
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                String mimetype = new MimetypesFileTypeMap().getContentType(source);
                String type = mimetype.split("/")[0];
                if (type.equals("image")) {
                    imageUrl = newFile.getName();
                    notif.setText("image téléchargé");
                    Image image = new Image(new URL("http://localhost:/pi/images/" + imageUrl).openStream());
                    imageview.setImage(image);
                }
                HttpEntity resEntity = response.getEntity();

                EntityUtils.consume(resEntity);
                newFile.delete();
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }

    }

    @FXML
    private void modifier(ActionEvent event) throws SQLException, IOException {
        AnnonceService serv = new AnnonceService();
        if (pays.getValue() == null || pays.getValue().isEmpty()) {
            error.setText("entrer un pays");
        } else if (age.getText() == null || age.getText().isEmpty()) {
            error.setText("entrer votre age");
        } else if (personne.getText() == null || personne.getText().isEmpty()) {
            error.setText("entrer nombre de personnes");
        } else if (Integer.parseInt(age.getText()) < 12) {
            error.setText("désolé interdit au moins de 12 ans");
        } else if (Integer.parseInt(personne.getText()) <= 0) {
            error.setText("nombre de personne doit être positive");
        } else if (expiration.getValue() == null) {
            error.setText("entrer une date d'expiration");
        } else if (masculin.selectedProperty().get() == false && feminin.selectedProperty().get() == false) {
            error.setText("cocher un genre");
        } else {
            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            Date datenow = new Date(System.currentTimeMillis());
            Date expirationDate = java.sql.Date.valueOf(expiration.getValue());
            if (expirationDate.getTime() < datenow.getTime()) {
                error.setText("date expiré");
            } else {
                error.setText("");
                String sexe;
                if (masculin.selectedProperty().get() == true && feminin.selectedProperty().get() == true) {
                    sexe = "both";
                } else if (masculin.selectedProperty().get() == true) {
                    sexe = "masculin";
                } else {
                    sexe = "feminin";
                }
                if (description.getText() == null) {
                    description.setText("");
                }
                String dateannonce = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                Annonce annonce = new Annonce(id_annonce, u.getId(), dateannonce, description.getText(), Integer.parseInt(personne.getText()), sexe, Integer.parseInt(age.getText()), expiration.getValue().toString(), imageUrl, "l", pays.getValue());
                serv.ModifierAnnonce(annonce);
                Parent root2 = FXMLLoader.load(getClass().getResource("/mnd/gui/annonces.fxml"));
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mnd/gui/Ui.fxml"));
                Parent root = (Parent) fxmlLoader.load();
                UiController controller = fxmlLoader.<UiController>getController();
                controller.setParent(root2);
                Scene scene = new Scene(root);
                Stage stage = (Stage) ap.getScene().getWindow();
                stage.setScene(scene);
                stage.show();
            }
        }
    }

    @FXML
    private void effacer(ActionEvent event) throws SQLException, IOException {
        AnnonceService serv = new AnnonceService();
        serv.supprimerAnnonceParid(id_annonce);
        Reservation_annonceService re = new Reservation_annonceService();
        re.deleteByIdAnnonce(id_annonce);
        Parent root2 = FXMLLoader.load(getClass().getResource("/mnd/gui/annonces.fxml"));
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/mnd/gui/Ui.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        UiController controller = fxmlLoader.<UiController>getController();
        controller.setParent(root2);
        Scene scene = new Scene(root);
        Stage stage = (Stage) ap.getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
