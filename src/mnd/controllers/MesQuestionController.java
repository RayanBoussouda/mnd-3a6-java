/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import mnd.controllers.AuthentificationController;
import mnd.entities.Question;
import mnd.entities.User;
import mnd.services.QuestionService;
import mnd.services.ReponseService;
import mnd.services.UserService;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.textfield.TextFields;

/**
 * FXML Controller class
 *
 * @author Moataz
 */
public class MesQuestionController implements Initializable {
User nu;
    User u = AuthentificationController.getInstance().getUser();
    UserService us;
    @FXML
    private Button poster;
    
    @FXML
    private TableView<Question> tableQuestion;
    @FXML
    private TableColumn<Question,String> question;
    @FXML
    private TableColumn<Question,String> date;
    @FXML
    private TableColumn<Question,String> nb_reponse;
    
    private ObservableList<Question> list ;
    @FXML
    private Button effacer;
    int questionSelected;
    @FXML
    private JFXTextField questionField;
    @FXML
    private FontAwesomeIcon icon;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
 
       refresh();
    }

    @FXML
    private void validerQuestion(ActionEvent event) {
        QuestionService qs = new QuestionService();

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        String contenu = questionField.getText();
        if(contenu.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez saisir un question");
            alert.showAndWait();
        }
        else 
        {
        Question q = new Question(u.getId(),contenu,timeStamp);
        qs.addQuestion(q);
         Notifications notificationBuilder = Notifications.create()
                    .title("Question")
                    .text("votre question a eté ajouté")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT)
                    ;
        notificationBuilder.darkStyle();
        notificationBuilder.showInformation();
        refresh();  
    }
    }
    public void refresh(){
        QuestionService qs = new QuestionService();
         ReponseService r = new ReponseService();
         ArrayList<Question> q = new ArrayList<Question>();
        q = qs.afficherContenuQuestion();
        TextFields.bindAutoCompletion(questionField, q);
      list = FXCollections.observableArrayList(qs.afficherMesQuestion(u.getId()));
      
      question.setCellValueFactory(new PropertyValueFactory<>("contenu"));
      date.setCellValueFactory(new PropertyValueFactory<>("date_publication"));
      nb_reponse.setCellValueFactory(i -> {
                int b = r.afficherReponse(i.getValue().getId()).size();
                SimpleStringProperty aa = new SimpleStringProperty(Integer.toString(b));
                return aa;
            });
      tableQuestion.setItems(list);
    }

    @FXML
    private void effacerAction(ActionEvent event) {
      
        QuestionService qs = new QuestionService();
        Question question = qs.afficherMaQuestion(questionSelected);
        qs.supprimerQuestion(question);
        Notifications notificationBuilder = Notifications.create()
                    .title("Question")
                    .text("votre question a eté supprimé")
                    .graphic(null)
                    .hideAfter(Duration.seconds(5))
                    .position(Pos.TOP_RIGHT)
                    ;
        notificationBuilder.darkStyle();
        notificationBuilder.showWarning();
       
        refresh();
    }

    @FXML
    private void getQuestion(MouseEvent event) {
         QuestionService qs = new QuestionService();
         questionSelected=tableQuestion.getSelectionModel().getSelectedItem().getId();
        
    }

    
    
}

    

    

