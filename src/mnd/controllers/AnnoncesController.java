/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import mnd.entities.User;
import mnd.services.AnnonceService;

/**
 * FXML Controller class
 *
 * @author kaisf
 */
public class AnnoncesController implements Initializable {

    User u = AuthentificationController.getInstance().getUser();

    @FXML
    private BorderPane borderPane;
    @FXML
    private GridPane gridPane;
    @FXML
    private VBox vbox;
    @FXML
    private VBox vbox2;
    @FXML
    private VBox vbox1;
    @FXML
    private VBox vbox3;
    private Label label;
    @FXML
    private VBox vboxtext;
    @FXML
    private FontAwesomeIcon icon;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            double textWidth = icon.getLayoutBounds().getWidth();
            // Define the Durations
            Duration startDuration = Duration.ZERO;
            Duration endDuration = Duration.seconds(5);

            // Create the start and end Key Frames
            KeyValue startKeyValue = new KeyValue(icon.translateXProperty(), 0);
            KeyFrame startKeyFrame = new KeyFrame(startDuration, startKeyValue);
            KeyValue endKeyValue = new KeyValue(icon.translateXProperty(), -1.0 * textWidth+2400);
            KeyFrame endKeyFrame = new KeyFrame(endDuration, endKeyValue);

            // Create a Timeline
            Timeline timeline = new Timeline(startKeyFrame, endKeyFrame);
            // Let the animation run forever
            timeline.setCycleCount(Timeline.INDEFINITE);
            // Run the animation
            timeline.play();
            AnnonceService serv = new AnnonceService();
            if (serv.getAnnonceParuserid(u.getId()).size() == 0) {
                vbox1.setStyle("-fx-background-color: #72726e");
            } else {
                vbox1.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        vbox1.setStyle("-fx-background-color: #ffd018");
                    }
                });
                vbox1.setOnMouseExited(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent arg0) {
                        vbox1.setStyle("-fx-background-color: #ffae19");
                    }
                });
            }
            vbox2.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    vbox2.setStyle("-fx-background-color: #ffd018");
                }
            });
            vbox2.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    vbox2.setStyle("-fx-background-color: #ffae19");
                }
            });
            vbox3.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    vbox3.setStyle("-fx-background-color: #ffd018");
                }
            });
            vbox3.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent arg0) {
                    vbox3.setStyle("-fx-background-color: #ffae19");
                }
            });
        } catch (SQLException ex) {
            Logger.getLogger(AnnoncesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void rechercher(MouseEvent event) throws IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/RechercherAnnonce.fxml"));
        borderPane.setCenter(root);
    }

    @FXML
    private void mesAnnonces(MouseEvent event) throws IOException, SQLException {
        AnnonceService serv = new AnnonceService();
        if (serv.getAnnonceParuserid(u.getId()).size() == 1) {
            Parent root = null;
            root = FXMLLoader.load(getClass().getResource("/mnd/gui/MesAnnonces.fxml"));
            borderPane.setCenter(root);
        }
    }

    @FXML
    private void ajouterAnnonce(MouseEvent event) throws IOException {
        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("/mnd/gui/AjouterAnnonce.fxml"));
        borderPane.setCenter(root);
    }
    void setParent(Parent root) {
         borderPane.setCenter(root);
    }

}
