/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mnd.controllers.AuthentificationController;
import mnd.entities.User;
import static mnd.main.Mnd.stage;
import mnd.services.UserService;

/**
 * FXML Controller class
 *
 * @author Camilia
 */
public class AcueilController implements Initializable {

    User u;
    UserService us;
    @FXML
    private AnchorPane anchorPane;

    @FXML
    private VBox vv;
    public static AcueilController acueilController;
    private User user;
    @FXML
    private JFXButton filtreB;
    @FXML
    private JFXTextField nomT;
    @FXML
    private JFXCheckBox femmeC;
    @FXML
    private JFXCheckBox hommeC;
    @FXML
    private JFXTextField ageT;
    @FXML
    private JFXComboBox paysC;
    private final ObservableList<String> listePays = FXCollections.observableArrayList("","Tunisie", "France", "Espagne", "Allemagne", "Turquie", "USA", "Angleterre");
    ArrayList<User> ul_filtrer;
    ArrayList<User> ul;

    public AcueilController() {
        acueilController = this;
    }

    public static AcueilController getInstance() {
        return acueilController;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User u) {
        System.out.println(u);
        user=u;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        u = new User();
        try {
            us = new UserService();
        } catch (SQLException ex) {
            Logger.getLogger(AcueilController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ul = new ArrayList(us.getAllUsers());
          paysC.setValue("");
        paysC.setItems(listePays);
        updateProfile(ul);
       

    }

    @FXML
    private void filtrer(ActionEvent event) {
        List<User> ul_filtrer = ul;
        
        if (hommeC.selectedProperty().get() == true && femmeC.selectedProperty().get() == false) {
            ul_filtrer = ul_filtrer.stream().filter(e -> e.getGenre().equals("Homme")).collect(Collectors.toList());
        } else if (femmeC.selectedProperty().get() == true && hommeC.selectedProperty().get() == false) {
            ul_filtrer = ul_filtrer.stream().filter(e -> e.getGenre().equals("Femme")).collect(Collectors.toList());
        }
        if (!nomT.getText().equals("")) {
            ul_filtrer = ul_filtrer.stream().filter(e -> e.getUsername().contains(nomT.getText())).collect(Collectors.toList());
        }
        if (!ageT.getText().equals("")) {
            ul_filtrer = ul_filtrer.stream().filter(e -> e.getAge() == Integer.parseInt(ageT.getText())).collect(Collectors.toList());
        }
//        if (!paysC.getValue().equals("")) {
//           ul_filtrer = ul_filtrer.stream().filter(e -> e.getCountry().contains((String)paysC.getValue())).collect(Collectors.toList());       
//         }
        ul_filtrer.stream().forEach(System.out::println);
        updateProfile((ArrayList<User>) ul_filtrer);
    }

    public void updateProfile(ArrayList<User> ul) {
        vv.getChildren().clear();
        Label lb = new Label();
        vv.setPadding(new Insets(0, 0, 0, 0));
        String v;
        for (int i = 0; i < ul.size(); i++) {
            HBox hb = new HBox();

            v = ul.get(i).getUsername();
            Image image = null;
            try {
                if (ul.get(i).getPhoto_membre().equals(""))
                    image = new Image(new URL("http://localhost/pi/images/default.jpg").openStream());
                else
                 image = new Image(new URL("http://localhost/pi/images/" + ul.get(i).getPhoto_membre()).openStream());
            } catch (IOException ex) {
                Logger.getLogger(AcueilController.class.getName()).log(Level.SEVERE, null, ex);
            }

            ImageView im = new ImageView(image);
            im.setFitWidth(50);

            im.setFitHeight(50);
            lb = new Label(v);
            hb.getChildren().add(im);
            hb.getChildren().add(lb);
            hb.setMinWidth(800);
            vv.setId("vbox_style");
            hb.setId("hbox");
            im.setId("im_style");
            lb.setId("label_style");
            vv.getChildren().addAll(hb);
            final String username = ul.get(i).getUsername();
            User u = ul.get(i);
            hb.setOnMouseEntered(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    hb.setStyle("-fx-background-color:#cfdbd5;");
                }
            });
            hb.setOnMouseExited(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    hb.setStyle("-fx-background-color:white");
                }
            });
            hb.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent t) {
                    user = u;
                    Parent root;
                    Stage stage = (Stage) hb.getScene().getWindow();
                    try {
                        root = FXMLLoader.load(getClass().getResource("/mnd/gui/Profile.fxml"));
                        stage.close();
                        stage = new Stage();
                        Scene scene = new Scene(root);
                        scene.setFill(Color.TRANSPARENT);
                        stage.setScene(scene);
                        stage.initStyle(StageStyle.TRANSPARENT);
                        stage.show();
                    } catch (IOException ex) {
                        Logger.getLogger(AcueilController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            });

        }
    }

}
