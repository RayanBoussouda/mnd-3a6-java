/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.services;

/**
 *
 * @author kaisf
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import mnd.entities.Reservation_annonce;
import mnd.utils.MyDBcon;

/**
 *
 * @author Wadii Chamakhi
 */
public class Reservation_annonceService {

    Connection cnx;

    public Reservation_annonceService() throws SQLException {
        cnx = MyDBcon.getInstance().getCnx();

    }

    public void ajouterReservation(Reservation_annonce reservation) {
        try {
            String req = "INSERT INTO `reservation_annonce`( `id_user`,`id_annonce`,`date_reservation`,`date_expiration`) VALUES (?,?,?,?)";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(reservation.getId_user()));
            pstm.setString(2, Integer.toString(reservation.getId_annonce()));
            pstm.setString(3, reservation.getDate_reservation());
            pstm.setString(4, reservation.getDate_expiration());
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void ModifierReservation(Reservation_annonce reservation, String id) {
        try {
            String req = "update `reservation_annonce` set `id_user`=?,`id_annonce`=?,`date_reservation`=?,`date_expiration`=? where id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(reservation.getId_user()));
            pstm.setString(2, Integer.toString(reservation.getId_annonce()));
            pstm.setString(3, reservation.getDate_reservation());
            pstm.setString(4, reservation.getDate_expiration());
            pstm.setString(5, Integer.toString(reservation.getId()));
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteById(int id) throws SQLException {
        try {
            String req = "DELETE FROM `reservation_annonce` WHERE id=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(id));
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteByIdAnnonce(int id) throws SQLException {
        try {
            String req = "DELETE FROM `reservation_annonce` WHERE id_annonce=?";
            PreparedStatement pstm = cnx.prepareStatement(req);
            pstm.setString(1, Integer.toString(id));
            pstm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AnnonceService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Reservation_annonce> getReservationsByIdAnnonce(int i) throws SQLException {
        ArrayList<Reservation_annonce> retour = new ArrayList<>();
        Statement stm = cnx.createStatement();
        String req = "SELECT * FROM reservation_annonce where id_annonce=?";
        PreparedStatement pstm = cnx.prepareStatement(req);
        pstm.setInt(1, i);
        ResultSet resultat = pstm.executeQuery();
        while (resultat.next()) {
            int id = Integer.parseInt(resultat.getString("id"));
            int id_user = Integer.parseInt(resultat.getString("id_user"));
            int id_annonce = Integer.parseInt(resultat.getString("id_annonce"));
            String date_reservation = resultat.getString("date_reservation");
            String date_expiration = resultat.getString("date_expiration");
            retour.add(new Reservation_annonce(id, id_user, id_annonce, date_reservation, date_expiration));
        }

        return retour;
    }
}
