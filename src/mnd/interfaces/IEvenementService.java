/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.interfaces;

import java.util.ArrayList;
import java.util.List;
import mnd.entities.Evenement;
import mnd.entities.Ratings;
import mnd.entities.User;

/**
 *
 * @author Steve
 */
public interface IEvenementService {

    public void add(Evenement t);

    public void update(Evenement t, int id);

    public void remove(Evenement t);

    public List<Evenement> findAll();

    public Evenement findById(int r);

    public void addRes(int id_evenement, int id_user);

    public void rating(Ratings r);

    public List<User> listRes(int id_evenement);

    public boolean testrating(int id_evenement, int id_user);

}
