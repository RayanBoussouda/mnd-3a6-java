/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mnd.interfaces;

import java.util.List;
import mnd.entities.Commentaire;
import mnd.entities.Endroit;

/**
 *
 * @author Ahmed
 */
public interface ICommentaireServices {
    public void insertComment(Commentaire c);
    public void updateComment(int endroit_id, String contenu);
    public void deleteComment(Commentaire c);
    
    public List<Commentaire> listAllComm(Endroit e);


    
}
